# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals
from django.contrib.sites.models import Site
from django.db import models
from thecut.core.managers import QuerySetManager
from thecut.ordering import models as ordering
from thecut.publishing import models as publishing, querysets, utils
import warnings


OrderMixin = ordering.OrderMixin


class AbstractBaseResource(publishing.PublishableResource):
    """AbstractBaseResource is deprecated. Use PublishableResource instead.

    """

    QuerySet = querysets.PublishableResourceQuerySet

    class Meta(publishing.PublishableResource.Meta):
        abstract = True

    def __init__(self, *args, **kwargs):
        warnings.warn('AbstractBaseResource is deprecated - use '
                      'PublishableResource from thecut.publishing instead.',
                      DeprecationWarning, stacklevel=2)
        return super(AbstractBaseResource, self).__init__(*args, **kwargs)


class AbstractResource(publishing.Content):
    """AbstractResource is deprecated. Use Content instead.

    """

    QuerySet = querysets.ContentQuerySet

    class Meta(publishing.Content.Meta):
        abstract = True

    def __init__(self, *args, **kwargs):
        warnings.warn('AbstractResource is deprecated - use Content from '
                      'thecut.publishing instead.', DeprecationWarning,
                      stacklevel=2)
        return super(AbstractResource, self).__init__(*args, **kwargs)


class AbstractSiteResource(publishing.SiteContent):
    """AbstractSiteResource is deprecated. Use SiteContent instead.

    """

    QuerySet = querysets.SiteContentQuerySet

    class Meta(publishing.SiteContent.Meta):
        abstract = True

    def __init__(self, *args, **kwargs):
        warnings.warn('AbstractSiteResource is deprecated - use SiteContent '
                      'from thecut.publishing instead.', DeprecationWarning,
                      stacklevel=2)
        return super(AbstractSiteResource, self).__init__(*args, **kwargs)


class AbstractSiteResourceWithSlug(publishing.SiteContentWithSlug):
    """AbstractSiteResourceWithSlug is deprecated. Use SiteContentWithSlug
    instead.

    """

    QuerySet = querysets.SiteContentQuerySet

    class Meta(publishing.SiteContentWithSlug.Meta):
        abstract = True

    def __init__(self, *args, **kwargs):
        warnings.warn('AbstractSiteResourceWithSlug is deprecated - use '
                      'SiteContentWithSlug from thecut.publishing instead.',
                      DeprecationWarning, stacklevel=2)
        return super(AbstractSiteResourceWithSlug, self).__init__(*args,
                                                                  **kwargs)


# The models below do not exist in ``thecut.publishing``, but have been kept
# here for backwards-compatibility


class AbstractSitesResource(publishing.Content):
    """Deprecated abstract resource model with a relationship to many sites.

    """

    sites = models.ManyToManyField('sites.Site', null=True, blank=True)

    objects = QuerySetManager()

    class Meta(publishing.Content.Meta):
        abstract = True

    class QuerySet(querysets.ContentQuerySet):

        def current_site(self):
            """Return objects for the current site."""
            site = Site.objects.get_current()
            return self.filter(sites=site)

    def __init__(self, *args, **kwargs):
        warnings.warn('AbstractSitesResource is deprecated.',
                      DeprecationWarning, stacklevel=2)
        return super(AbstractSitesResource, self).__init__(*args, **kwargs)


class AbstractSitesResourceWithSlug(AbstractSitesResource):
    """"Deprecated abstract sites resource model with a unique slug.

    """

    slug = models.SlugField(unique=True)

    objects = QuerySetManager()

    class Meta(AbstractSitesResource.Meta):
        abstract = True

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = utils.generate_unique_slug(
                self.title, self.__class__.objects.all())
        return super(AbstractSitesResourceWithSlug, self).save(*args, **kwargs)

    def __init__(self, *args, **kwargs):
        warnings.warn('AbstractSitesResourceWithSlug is deprecated.',
                      DeprecationWarning, stacklevel=2)
        return super(AbstractSitesResourceWithSlug, self).__init__(*args,
                                                                   **kwargs)
