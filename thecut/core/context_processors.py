# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals
from thecut.currentsite import context_processors as currentsite
import warnings


def current_site(*args, **kwargs):
    warnings.warn('thecut.core is deprecated - use thecut.currentsite '
                  'instead.', DeprecationWarning, stacklevel=2)
    return currentsite.current_site(*args, **kwargs)
