# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals
from thecut.authorship import utils as authorship
from thecut.publishing import utils as publishing
import warnings


def generate_unique_slug(text, model, queryset=None, iteration=0):
    warnings.warn('thecut.core is deprecated - use thecut.publishing instead.',
                  DeprecationWarning, stacklevel=2)
    if queryset is None:
        queryset = model.objects.all()
    return publishing.generate_unique_slug(text, queryset, iteration=iteration)


def get_website_user():
    warnings.warn('thecut.core is deprecated - use thecut.authorship instead.',
                  DeprecationWarning, stacklevel=2)
    return authorship.get_website_user()
