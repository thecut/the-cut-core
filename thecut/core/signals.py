# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals
from thecut.ordering import receivers as ordering
import warnings


def set_order(*args, **kwargs):
    warnings.warn('thecut.core is deprecated - use thecut.ordering instead.',
                  DeprecationWarning, stacklevel=2)
    return ordering.set_order(*args, **kwargs)
