# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals
from django import forms
from thecut.ordering.forms import OrderMixin
import warnings


class ModelAdminForm(OrderMixin, forms.ModelForm):

    def __init__(self, *args, **kwargs):
        warnings.warn('thecut.core is deprecated - use thecut.publishing '
                      'instead.', DeprecationWarning, stacklevel=2)
        return super(ModelAdminForm, self).__init__(*args, **kwargs)
