# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals
from django.contrib import admin
from thecut.authorship import admin as authorship
from thecut.ordering import admin as ordering
import warnings


class ModelAdmin(authorship.AuthorshipMixin, admin.ModelAdmin):

    def __init__(self, *args, **kwargs):
        warnings.warn('thecut.core is deprecated - use thecut.authorship '
                      'instead.', DeprecationWarning, stacklevel=2)
        return super(ModelAdmin, self).__init__(*args, **kwargs)


class ReorderMixin(ordering.ReorderMixin):

    def __init__(self, *args, **kwargs):
        warnings.warn('thecut.core is deprecated - use thecut.ordering '
                      'instead.', DeprecationWarning, stacklevel=2)
        return super(ReorderMixin, self).__init__(*args, **kwargs)
